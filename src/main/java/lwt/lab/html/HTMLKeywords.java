/**
 *
 */
package lwt.lab.html;

/**
 * @author Robert
 *
 */
public class HTMLKeywords {

	// HTML elements
	public static final String HTML = "html";
	public static final String BODY = "body";
	public static final String HEAD = "head";
	public static final String LINK = "link";
	public static final String TITLE = "title";
	public static final String INPUT = "input";
	public static final String FORM = "form";
	public static final String UL = "ul";
	public static final String OL = "ol";
	public static final String LI = "li";
	public static final String TABLE = "table";
	public static final String TH = "th";
	public static final String TR = "tr";
	public static final String TD = "td";
	public static final String P = "p";
	public static final String A = "a";

	//HTML input types
	public static final String TEXT = "text";
	public static final String SUBMIT = "submit";
	public static final String TEXTAREA = "textarea";

	// HTML attributes
	public static final String NAME = "name";
	public static final String ACTION = "action";
	public static final String METHOD = "method";
	public static final String TYPE = "type";
	public static final String SRC = "src";
	public static final String HREF = "href";
	public static final String REL = "rel";

}
