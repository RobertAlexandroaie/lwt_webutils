package lwt.lab.html.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lwt.lab.html.model.A;
import lwt.lab.html.model.Body;
import lwt.lab.html.model.Form;
import lwt.lab.html.model.HTMLElement;
import lwt.lab.html.model.Head;
import lwt.lab.html.model.Html;
import lwt.lab.html.model.Li;
import lwt.lab.html.model.Link;
import lwt.lab.html.model.P;
import lwt.lab.html.model.Submit;
import lwt.lab.html.model.Table;
import lwt.lab.html.model.Td;
import lwt.lab.html.model.Textarea;
import lwt.lab.html.model.Th;
import lwt.lab.html.model.Title;
import lwt.lab.html.model.Ul;
import lwt.lab.html.model.XMLElement;

/**
 * Some simple time savers. Static methods.
 */

public class HTMLUtils {

	private HTMLUtils() {

	}

	private static String HTML5Doctype() {

		return "<!DOCTYPE html>\n";
	}

	/**
	 * @param title
	 *
	 * @return
	 */
	public static String titledHTML(String title) {

		return titleBodyStyleHTML(title, null, "../css/styles.css");
	}

	/**
	 * @return
	 */
	public static String cleanHTML() {

		return titleBodyStyleHTML(null, null, "../css/styles.css");
	}

	/**
	 * @param title
	 * @param body
	 * @param cssPath
	 *
	 * @return
	 */
	public static String titleBodyStyleHTML(String title, String body, String cssPath) {

		StringBuilder htmlBuilder = new StringBuilder();

		Body bodyElement = new Body(body != null ? body : "");
		Title titleElement = new Title(title != null ? title : "");
		Link linkElement = new Link(cssPath);
		Head headElement = new Head();
		headElement.addChild(titleElement);
		headElement.addChild(linkElement);
		Html htmlElement = new Html();
		htmlElement.addChild(headElement);
		htmlElement.addChild(bodyElement);
		htmlBuilder.append(HTML5Doctype()).append(htmlElement.toHTML());
		return htmlBuilder.toString();
	}

	/**
	 * @param title
	 * @param body
	 *
	 * @return
	 */
	public static String titleBodyHTML(String title, String body) {

		return titleBodyStyleHTML(title, body, "./css/styles.css");
	}

	/**
	 * @param data
	 *
	 * @return
	 */
	public static String createUL(String[] data) {

		Ul ulElement = new Ul();
		addChildren(data, ulElement, Li.class);
		return ulElement.toHTML();
	}

	/**
	 * @param data
	 * @param parent
	 * @param childType
	 */
	protected static void addChildren(String[] data, HTMLElement parent, Class<?> childType) {

		for (String value : data) {
			addChild(parent, value, childType);
		}
	}

	/**
	 * @param parent
	 * @param elementData
	 * @param childType
	 */
	protected static void addChild(HTMLElement parent, String elementData, Class<?> childType) {

		Constructor<?> constructor;
		try {
			constructor = childType.getConstructor(String.class);
			XMLElement childElement = (XMLElement) constructor.newInstance(elementData);
			parent.addChild(childElement);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param data
	 * @param headerLabels
	 *
	 * @return
	 */
	public static String createTable(String[][] data, String[] headerLabels) {

		List<XMLElement> ths = createThElements(headerLabels);
		List<XMLElement> trs = createTrElements(data);

		Table table = new Table();
		table.addChildren(ths);
		table.addChildren(trs);
		return table.toHTML();
	}

	/**
	 * @param headerLabels
	 *
	 * @return
	 */
	protected static List<XMLElement> createThElements(String[] headerLabels) {

		List<XMLElement> ths = new ArrayList<>();
		if (headerLabels != null) {
			for (int i = 0; i < headerLabels.length; i++) {
				ths.add(new Th(headerLabels[i]));
			}
		}
		return ths;
	}

	private static List<XMLElement> createTrElements(String[][] data) {

		List<XMLElement> trs = new ArrayList<>();
		List<XMLElement> tds;
		for (int i = 0; i < data.length; i++) {
			tds = new ArrayList<>();
			for (int j = 0; j < data[i].length; j++) {
				tds.add(new Td(data[i][j]));
			}
			trs.get(i).addChildren(trs);
		}
		return trs;
	}

	/**
	 * @param paragraph
	 *
	 * @return
	 */
	public static String createP(String paragraph) {

		return new P(paragraph).toHTML();
	}

	/**
	 * @param content
	 * @param attributes
	 *
	 * @return
	 */
	public static String createA(String content, Map<String, String> attributes) {

		A anchor = new A(content);
		anchor.addAttributes(attributes);
		return anchor.toHTML();
	}

	/**
	 * @param content
	 * @param attributes
	 *
	 * @return
	 */
	public static String createTextarea(String content, Map<String, String> attributes) {

		Textarea textarea = new Textarea(content);
		textarea.addAttributes(attributes);
		return textarea.toHTML();
	}

	/**
	 * @param content
	 * @param attributes
	 *
	 * @return
	 */
	public static String createFrom(String content, Map<String, String> attributes) {

		Form form = new Form(content);
		form.addAttributes(attributes);
		return form.toHTML();
	}

	/**
	 * @param content
	 * @param attributes
	 *
	 * @return
	 */
	public static String createSubmitButton(String content, Map<String, String> attributes) {

		Submit submit = new Submit(content);
		submit.addAttributes(attributes);
		return submit.toHTML();
	}

	/**
	 * @return
	 */
	public static String createDefaultSubmitButton() {

		Map<String, String> attributes = new HashMap<>();
		attributes.put("type", "submit");
		String submitButton = HTMLUtils.createSubmitButton("Submit", attributes);
		return submitButton;
	}

	/**
	 * @param textareaNameLabel
	 *
	 * @return
	 */
	public static String createDefaultTextarea(String textareaNameLabel) {

		Map<String, String> attributes = new HashMap<>();
		attributes.put("rows", "6");
		attributes.put("cols", "40");
		attributes.put("name", textareaNameLabel);
		String textarea = HTMLUtils.createTextarea("", attributes);
		return textarea;
	}

	/**
	 * @param textareaNameLabel
	 * @param action
	 * @param method
	 *
	 * @return
	 */
	public static String createTextareaForm(String textareaNameLabel, String action, String method) {

		String textarea = HTMLUtils.createDefaultTextarea(textareaNameLabel);
		String submitButton = HTMLUtils.createDefaultSubmitButton();
		Map<String, String> attributes = new HashMap<>();
		attributes.put("action", action);
		attributes.put("method", method);
		String form = HTMLUtils.createFrom(textarea + submitButton, attributes);
		return form;
	}

	/**
	 * @param nameParamLabel
	 * @param surnameParamLabel
	 * @param emailParamLabel
	 * @param action
	 * @param method
	 *
	 * @return
	 */
	public static String createPersonForm(String nameParamLabel, String surnameParamLabel, String emailParamLabel,
			String action, String method) {

		String nameInput = HTMLUtils.createLabeledTextInput("Name", nameParamLabel);
		String surnameInput = HTMLUtils.createLabeledTextInput("Surame", surnameParamLabel);
		String emailInput = HTMLUtils.createLabeledTextInput("Email", nameParamLabel);
		String submitButton = HTMLUtils.createDefaultSubmitButton();
		Map<String, String> attributes = new HashMap<>();
		attributes.put("action", action);
		attributes.put("method", method);
		String form = HTMLUtils.createFrom(
				nameInput + "<br>" + surnameInput + "<br>" + emailInput + "<br>" + submitButton, attributes);
		return form;
	}

	/**
	 * @param label
	 * @param paramLabel
	 *
	 * @return
	 */
	private static String createLabeledTextInput(String label, String paramLabel) {

		Map<String, String> attributes = new HashMap<>();
		attributes.put("type", "text");
		attributes.put("name", paramLabel);
		String textarea = HTMLUtils.createLabeledInputText("", attributes);
		return textarea;
	}

	/**
	 * @param label
	 * @param attributes
	 *
	 * @return
	 */
	private static String createLabeledInputText(String label, Map<String, String> attributes) {

		return label + " " + HTMLElementBuilder.createElement(null);
	}

}
