/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.HEAD;

/**
 * @author Robert
 *
 */
public class Head extends GenericHTMLElement {

	public Head() {

		super();
		this.tagName = HEAD;
	}
}
