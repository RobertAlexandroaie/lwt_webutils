/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TEXTAREA;

/**
 * @author Robert
 *
 */
public class Textarea extends Input {

	public Textarea() {

		super();
		setType(TEXTAREA);
	}

	public Textarea(String value) {

		this();
		this.value = value;
	}

}
