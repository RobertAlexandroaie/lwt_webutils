/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.LI;

/**
 * @author Robert
 *
 */
public class Li extends GenericHTMLElement {

	/**
	 *
	 */
	public Li(String value) {

		super();
		this.tagName = LI;
		this.value = value;
	}

}
