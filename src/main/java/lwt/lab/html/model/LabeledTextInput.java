/**
 *
 */
package lwt.lab.html.model;

/**
 * @author Robert
 *
 */
public class LabeledTextInput extends Text {

	private String label;

	/**
	 *
	 * @param label
	 */
	public LabeledTextInput(String label) {

		super("");
		this.label = label;
	}

	/**
	 * @param name
	 * @param label
	 */
	public LabeledTextInput(String name, String label) {

		super(name);
		this.label = label;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {

		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {

		this.label = label;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.model.GenericHTMLElement#toHTML()
	 */
	@Override
	public String toHTML() {

		return label + " " + super.toHTML();
	}

}
