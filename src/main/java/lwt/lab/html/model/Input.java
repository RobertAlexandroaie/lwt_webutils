/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.INPUT;
import static lwt.lab.html.HTMLKeywords.NAME;
import static lwt.lab.html.HTMLKeywords.TYPE;

/**
 * @author Robert
 *
 */
public class Input extends GenericHTMLElement {

	public Input() {

		super();
		tagName = INPUT;
	}

	public Input(String value) {

		this();
		this.value = value;
	}

	public String getType() {

		return getAttribute(TYPE);
	}

	public String getName() {

		return getAttribute(NAME);
	}

	public String setType(String type) {

		return setAttribute(TYPE, type);
	}

	public String setName(String name) {

		return setAttribute(NAME, name);
	}
}
