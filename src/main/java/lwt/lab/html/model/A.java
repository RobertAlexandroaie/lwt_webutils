/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.A;

/**
 * @author Robert
 *
 */
public class A extends GenericHTMLElement {

	/**
	 *
	 */
	public A() {

		super();
		this.tagName = A;
	}

	public A(String value) {

		this();
		this.value = value;
	}
}
