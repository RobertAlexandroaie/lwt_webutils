/**
 *
 */
package lwt.lab.html.model;

/**
 * @author Robert
 *
 */
public class Text extends Input {

	public Text(String name) {

		super();
		setType("text");
		setName(name);
	}
}
