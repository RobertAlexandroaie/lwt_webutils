/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.UL;

/**
 * @author Robert
 *
 */
public class Ul extends GenericHTMLElement {

	/**
	 *
	 */
	public Ul() {

		super();
		this.tagName = UL;
	}

	public Ul(String value) {

		this();
		this.value = value;
	}
}
