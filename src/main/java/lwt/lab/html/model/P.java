/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.P;

/**
 * @author Robert
 *
 */
public class P extends GenericHTMLElement {

	public P() {

		super();
		this.tagName = P;
	}

	public P(String value) {

		this();
		this.value = value;
	}

}
