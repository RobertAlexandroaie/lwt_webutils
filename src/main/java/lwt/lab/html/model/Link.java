/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.HREF;
import static lwt.lab.html.HTMLKeywords.LINK;
import static lwt.lab.html.HTMLKeywords.REL;
import static lwt.lab.html.HTMLKeywords.TYPE;

/**
 * @author Robert
 *
 */
public class Link extends GenericHTMLElement {

	/**
	 *
	 */
	public Link() {

		super();
		this.tagName = LINK;
		this.container = false;
		this.setRel("stylesheet");
		this.setType("text/css");
	}

	/**
	 * @param href
	 */
	public Link(String href) {

		this();
		this.setHref(href);
	}

	public String setRel(String rel) {

		return setAttribute(REL, rel);
	}

	public String setHref(String href) {

		return setAttribute(HREF, href);
	}

	public String setType(String type) {

		return setAttribute(TYPE, type);
	}

	public String getRel() {

		return getAttribute(REL);
	}

	public String getHref() {

		return getAttribute(HREF);
	}

	public String getType() {

		return getAttribute(TYPE);
	}
}
