/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TITLE;

/**
 * @author Robert
 *
 */
public class Title extends GenericHTMLElement {

	/**
	 *
	 */
	public Title() {

		super();
		this.tagName = TITLE;
	}

	/**
	 *
	 * @param value
	 */
	public Title(String value) {

		this();
		this.value = value;
	}
}
