/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TD;

/**
 * @author Robert
 *
 */
public class Td extends GenericHTMLElement {

	/**
	 * @param value
	 */
	public Td(String value) {

		super(value);
		this.tagName = TD;
	}

}
