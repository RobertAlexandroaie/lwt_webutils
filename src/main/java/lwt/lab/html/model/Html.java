/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.HTML;

/**
 * @author Robert
 *
 */
public class Html extends GenericHTMLElement {

	public Html() {

		super();
		this.tagName = HTML;
	}

}
