/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.ACTION;
import static lwt.lab.html.HTMLKeywords.FORM;
import static lwt.lab.html.HTMLKeywords.METHOD;

/**
 * @author Robert
 *
 */
public class Form extends GenericHTMLElement {

	public Form() {

		super();
		this.tagName = FORM;
	}

	public Form(String value) {

		this();
		this.value = value;
	}

	public String setAction(String action) {

		return setAttribute(ACTION, action);
	}

	public String setMethod(String method) {

		return setAttribute(METHOD, method);
	}

	public String getAction() {

		return getAttribute(ACTION);
	}

	public String getMethod() {

		return getAttribute(METHOD);
	}
}
