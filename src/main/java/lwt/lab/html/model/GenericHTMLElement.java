/**
 *
 */
package lwt.lab.html.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import lwt.lab.html.utils.HTMLElementBuilder;

/**
 * @author Robert
 *
 */
public abstract class GenericHTMLElement implements HTMLElement {

	protected String tagName;
	protected String value;
	private Map<String, String> attributes;
	private Collection<XMLElement> children;
	private HTMLElement parent;
	protected boolean container = true;

	/**
	 *
	 */
	public GenericHTMLElement() {

		attributes = new HashMap<>();
		children = new ArrayList<>();
	}

	public GenericHTMLElement(String value) {

		this();
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.HTMLElement#toHTML()
	 */
	@Override
	public String toHTML() {

		return HTMLElementBuilder.createElement(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.XMLElement#getTagName()
	 */
	@Override
	public String getTagName() {

		return tagName;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#addChild()
	 */
	@Override
	public boolean addChild(XMLElement child) {

		boolean result = false;
		HTMLElement element = null;
		if (child instanceof HTMLElement) {
			element = (HTMLElement) child;
			result = children.add(element);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#getAttributes()
	 */
	@Override
	public Map<String, String> getAttributes() {

		return attributes;
	}

	protected String getAttribute(String name) {

		return attributes.get(name);
	}

	protected String setAttribute(String name, String value) {

		attributes.remove(name);
		return attributes.put(name, value);
	}

	/**
	 * @return the children
	 */
	public Collection<XMLElement> getChildren() {

		return children;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(Collection<XMLElement> children) {

		this.children = children;
	}

	/**
	 * @return the parent
	 */
	public HTMLElement getParent() {

		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(HTMLElement parent) {

		this.parent = parent;
	}

	/**
	 * @return the value
	 */
	@Override
	public String getValue() {

		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {

		this.value = value;
	}

	/**
	 * @param attributes
	 *            the attributes to set
	 */
	public void addAttributes(Map<String, String> attributes) {

		for (String key : attributes.keySet()) {
			setAttribute(key, attributes.get(key));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#isContainable()
	 */
	@Override
	public boolean isContainer() {

		return container;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#addChildren(java.util.Collection)
	 */
	@Override
	public boolean addChildren(Collection<XMLElement> children) {

		return this.children.addAll(children);
	}

}
