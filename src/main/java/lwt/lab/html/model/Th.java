/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TH;

/**
 * @author Robert
 *
 */
public class Th extends GenericHTMLElement {

	public Th() {

		super();
		this.tagName = TH;
	}

	public Th(String value) {

		this();
		this.value = value;
	}

}
